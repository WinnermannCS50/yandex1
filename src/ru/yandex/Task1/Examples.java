package ru.yandex.Task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
/**
 * Создание файла
 * Запись в файл
 * Чтение из файла
 */

/**
 * A. A+B 1
 *
 * Ограничение времени	2 секунды
 * Ограничение памяти	64Mb
 * Ввод	стандартный ввод или input.txt
 * Вывод	стандартный вывод или output.txt
 *
 * Даны два числа
 * A и B.
 * Вам нужно вычислить их сумму
 * A+B.
 * В этой задаче для работы с входными и выходными данными вы можете использовать и файлы и потоки на ваше усмотрение.
 *
 * Формат ввода
 * Первая строка входа содержит числа A и B (−2⋅109≤A,B≤2⋅109) разделенные пробелом
 *
 * Формат вывода
 * В единственной строке выхода выведите сумму чисел A+B
 */

public class Examples {
    public static final String EX1PATH = "D:\\SpringStudy\\Yandex1\\resources\\ex1.txt";
    public static final String EX2PATH = "D:\\SpringStudy\\Yandex1\\resources\\ex2.txt";
    public static final String EX3PATH = "D:\\SpringStudy\\Yandex1\\resources\\ex3.txt";
    public static final String OUTPUTEX1PATH = "D:\\SpringStudy\\Yandex1\\resources\\outputEx1.txt";
    public static final String OUTPUTEX2PATH = "D:\\SpringStudy\\Yandex1\\resources\\outputEx2.txt";
    public static final String OUTPUTEX3PATH = "D:\\SpringStudy\\Yandex1\\resources\\outputEx3.txt";
    static int sum1;
    static int sum2;
    static int sum3;
    public static void main(String[] args) throws FileNotFoundException {
        ex1();
        outputEx1();
        ex2();
        outputEx2();
        ex3();
        outputEx3();
    }

    /**
     * Считавание значений из файла
     * @throws FileNotFoundException
     */
    public static void ex1() throws FileNotFoundException {
        System.out.println("Пример 1: Чтение из файла");

        /**
         * Создадим файл "resources/ex1.txt" с записью "2 2"
         */
        //Указаваем путь к файлу
        File file = new File(EX1PATH);
        //Создаем объект файла
        PrintWriter printWriter = new PrintWriter(file);
        //Записываем в файл значение "2 2"
        printWriter.println("2 2");
        //Закрываем printWriter
        printWriter.close();


        /**
         * Считать из файла "resources/ex1.txt" значение "2 2"
         * Сложить эти значения 2+2 и получить сумму 4
         * Создать файл "resources/outputEx1.txt" и записать в него полученную сумму
         */
        //Находит файл "resources/ex1.txt" по его пути
        Scanner scanner = new Scanner(file);
        //Считывает первую строку
        String line = scanner.nextLine();
        //Разделяет считанное между символами пробел " " на отдельные значения
        String[] numbersString = line.split(" ");

        //Создать массив из двух элементов
        int[] numbers = new int[2];
        //Счетчик изначально равен нулю
        int counter = 0;

        /**
         * number - временная переменная
         * numbersString - массив из строк
         * numbers[counter++] = Integer.parseInt(number) - конвертирует массив из строк в массив из цифр
         * counter++ - счетчик, в данном случае это индекс элемента в массиве numbers
         */
        for (String number:numbersString) {
            numbers[counter++] = Integer.parseInt(number);
        }

        //получить значение из нулевого элемента массива numbers и положить в меременную a
        int a = numbers[0];
        //получить значение из первого элемента массива numbers и положить в меременную b
        int b = numbers[1];
        //вывести значения элементов a и b через пробел
        System.out.println(a+" "+b);

        //Сумма полученных значений
        sum1 = a+b;
        System.out.println(sum1);

        //закрыть scanner
        scanner.close();

    }

    /**
     * Запись значений в файл
     * @throws FileNotFoundException
     */
    public static void outputEx1() throws FileNotFoundException {
        System.out.println("Пример 1: Запись в файл");
        //Указаваем путь к файлу
        File file = new File(OUTPUTEX1PATH);
        //Создаем объект файла "resources/outputEx1.txt" по указанному пути
        PrintWriter printWriter = new PrintWriter(file);
        //Записываем в файл "resources/outputEx1.txt" значение, полученное из суммы
        printWriter.println(sum1);
        System.out.println(sum1);
        //Закрываем printWriter
        printWriter.close();

    }


    public static void ex2() throws FileNotFoundException {
        System.out.println("Пример 2: Чтение из файла");

        File file = new File(EX2PATH);
        Scanner scanner = new Scanner(file);
        String line = scanner.nextLine();
        String[] numbersString = line.split(" ");

        int[] numbers = new int[2];
        int counter = 0;

        for (String number:numbersString) {
            numbers[counter++] = Integer.parseInt(number);
        }

        int a = numbers[0];
        int b = numbers[1];
        System.out.println(a+" "+b);

        sum2 = a+b;
        System.out.println(sum2);

        scanner.close();
    }

    public static void outputEx2() throws FileNotFoundException {
        System.out.println("Пример 2: Запись в файл");
        File file = new File(OUTPUTEX2PATH);
        PrintWriter printWriter = new PrintWriter(file);

        printWriter.println(sum2);
        System.out.println(sum2);
        printWriter.close();

    }


    public static void ex3() throws FileNotFoundException {
        System.out.println("Пример 3: Чтение из файла");

        File file = new File(EX3PATH);
        Scanner scanner = new Scanner(file);
        String line = scanner.nextLine();
        String[] numbersWords = line.split(" ");
        int[] numbers = new int[2];
        int counter = 0;

        for (String number:numbersWords) {
            numbers[counter++] = Integer.parseInt(number);
        }

        int a = numbers[0];
        int b = numbers[1];
        System.out.println(a+" "+b);

        sum3=a+b;
        System.out.println(sum3);

        scanner.close();

    }

    public static void outputEx3() throws FileNotFoundException {
        System.out.println("Пример 3: Запись в файл");
        File file = new File(OUTPUTEX3PATH);
        PrintWriter printWriter = new PrintWriter(file);

        printWriter.println(sum3);
        System.out.println(sum3);
        printWriter.close();

    }

}
