package ru.yandex.Task1;

import java.io.*;

public class WriteReadByteFile {
    public static final String EX1PATH = "D:\\SpringStudy\\Yandex1\\resources\\ex1.txt";
    public static void main(String[] args) throws IOException {

        //Указаваем путь к файлу
        File file = new File(EX1PATH);

        /**
         * Запись файла
         */

        //Если файл не существует
        if (!file.exists()) {
            //Создать новый файл
            file.createNewFile();
        }

        //Если файл существует
        if (file.exists()){
            //Содержимое файла
            String fileContent = "2 2";

            /**
             * Открыть поток для записи в файл и указать путь в какой файл.
             * true - данные в файл дописываются (добавляются)
             * false - файл перезаписывается
             */
            FileOutputStream fos = new FileOutputStream(EX1PATH, false);

            /**
             * запиписать String-данные в файл не получится, потому что
             * класс FileOutputStream работает с байтами
             *
             * fileContent.getBytes() - переводит String-данные в байты
             * Создадим массив байтов byte[] buffer и в него положим наши String-данные в виде байтов
             */
            byte[] buffer = fileContent.getBytes();
            System.out.println((char)buffer[0]);

            /**
             * Записываем в файл значение "2 2" из buffer
             * fos.write(buffer[0]); - так можно записать в файл нулевой элемент массива byte[] buffer
             */
            fos.write(buffer);
            //Закрываем поток FileOutputStream
            fos.close();
        }

        /**
         * Считываение файла
         */
        FileInputStream fin = new FileInputStream(EX1PATH);

        //
        int line = -1;

        /**
         * fin.read() - считывает данные
         * Пока (line=fin.read() не равно -1 цикл будет работать
         */
        while ((line=fin.read())!=-1){
            //char - выводит не число а символы
            System.out.print((char) line);
        }

        fin.close();
    }
}
