package ru.yandex.Task1;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WriteReadFile {
    public static final String EX1PATH = "D:\\SpringStudy\\Yandex1\\resources\\ex1.txt";

    public static void main(String[] args) throws IOException {

        //Указаваем путь к файлу
        File file = new File(EX1PATH);

        /**
         * Запись файла
         */

        //Если файл не существует
        if (!file.exists()) {
            //Создать новый файл
            file.createNewFile();
        }

        //Если файл существует
        if (file.exists()){
            //Создаем объект файла
            PrintWriter printWriter = new PrintWriter(file);
            //Записываем в файл значение "2 2"
            printWriter.println("2 2");
            //Закрываем printWriter
            printWriter.close();
        }


        /**
         * Считываение файла
         */

        //Находит файл "resources/ex1.txt" по его пути
        BufferedReader bufferedReader = new BufferedReader(new FileReader(EX1PATH));

        //Считывает первую строку
        String line = bufferedReader.readLine();

        //Разделяет считанное между символами пробел " " на отдельные значения
        //Arrays.asList(line.split(" ")) - переводит массив в list
        List<String> numbersString = Arrays.asList(line.split(" "));

        //Создать лист чисел
        List<Integer> numbers = new ArrayList<>();
        //Счетчик изначально равен нулю
        Integer counter = 0;

        /**
         * number - временная переменная
         * numbersString - лист из строк
         * numbers.add(counter++,Integer.parseInt(number)); - конвертирует лист из строк в лист из цифр и ложит в numbers = new ArrayList<>();
         * counter++ - счетчик, в данном случае это индекс элемента в листе numbers
         */
        for (String number : numbersString) {
            numbers.add(counter++, Integer.parseInt(number));
        }

        //получить значение из нулевого элемента массива numbers и положить в переменную a
        Integer a = numbers.get(0);
        //получить значение из первого элемента массива numbers и положить в переменную b
        Integer b = numbers.get(1);
        //вывести значения элементов a и b через пробел
        System.out.println(a + " " + b);
    }
}
